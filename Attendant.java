package week03;

public class Attendant
{

	public String pressButton(Seat seat)
	{

		seat.setLight();
		return "An attendant has been dispatched for seat number " + seat.id;

	}

}
