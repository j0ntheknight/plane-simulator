package week03;

public class Seat
{
	public int id; // This is the seat number
	// public int seatNum;
	public boolean lightOn;

	public Seat(int i)
	{
		id = i;
		lightOn = false;
	}

	public void setLight()
	{
		if(lightOn == false)
		{
			lightOn = true;
			return;
		}
		else
		{
			lightOn = false;
			return;
		}
	}

	@Override
	public String toString()
	{
		return lightOn == true
				? "Seat number [" + id + "] and it's light is on."
				: "Seat number [" + id + "] and it's light is off";
	}

	public String button()
	{
		this.setLight();
		return "The attendant light has been turned on for seat number " + this.id;
	}

}
