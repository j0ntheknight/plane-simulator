package week03;

import java.util.Iterator;
import java.util.Scanner;

//Design and write a program to simulate the “call attendant” button on a plane.
//
//Requirements: It should simulate five different seats. Each seat should have a button to “call” and a light that indicates that the attendant has been called. The attendant has a button that he or she can press to turn off all the lights.
//
//Design a sample UI and include it in your assignment (it can be drawn in Visio or a similar tool).
//
//Design the data structures (class or classes).
//
//Design the methods of the back-end (which the UI would use).
//
//Implement the back-end in Java, and create a simple test UI that uses it. The test UI doesn't have to be graphical... it's just a command-line application that uses the back-end.
//
//This is an exercise in layered architecture. The point is that the UI shouldn't contain the actual data. It should just have a reference to the class that implements the back end.

public class Main
{
	public static Plane session = new Plane(5);

	public static void main(String[] args)
	{

		Scanner input = new Scanner(System.in);

		System.out.println("Plane Simulator. Press h for help.");
		boolean execute = true;
		while(execute == true)
		{
			System.out.print("> ");
			String command = input.next();

			switch(command)
			{
				case "call":
					// Passenger presses button
					System.out.print("Which seat is pressing the call attendant buttton?\n>");
					int seatNum = input.nextInt();
					System.out.print(session.pressButton(seatNum));
					
					break;
				case "respond":
					// Attendant responds to buttons
					System.out.print(session.sendAttendant());

					break;
				case "list":
					// List all seats and whether their call light is on
					Iterator<Seat> iteratorList = session.getSeats();
					while(iteratorList.hasNext())
					{
						System.out.println(iteratorList.next());
					}
					break;
				case "h":
					// List command options
					System.out.println("call - Press button to call attendant");
					System.out.println(
							"respond - Attendants responds to all call lights");
					System.out.println(
							"list - List all seats and current call light status");
					break;

				case "exit":
					execute = false;
					break;
				default:
				 // If command option is not one listed
				 System.out.println("Unrecognized command - use h for help");
				 break;

			}

		}
		input.close();

	}

}
