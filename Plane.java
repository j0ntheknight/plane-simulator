package week03;

import java.util.ArrayList;
import java.util.Iterator;

public class Plane
{

	public static ArrayList<Seat> seats = new ArrayList<>();
	public static Attendant attendant = new Attendant();

	Plane(int numSeats)
	{
		int i = numSeats;
		initialize(i);
	}

	private void initialize(int numSeats)
	{
		for(int i = 0; i < numSeats; i++)
		{
			seats.add(new Seat(i + 1));
		}

		// seats.addAll(Arrays.asList(new Seat(), new Seat(), new Seat(), new
		// Seat(), new Seat()));

	}

	public Iterator<Seat> getSeats()
	{

		return seats.iterator();
	}

	public String pressButton(int seatNum)
	{
		String buffer = null;
		for(int i = 0; i < seats.size(); i++)
		{
			if(seats.get(i).id == seatNum)
			{
				if(buffer == null)
				{
					buffer = "";
				}				
				buffer += seats.get(i).button();
				buffer += "\n";

			}
		}
		if(buffer != null)
		{
			return buffer;
		}
		else
		{
			buffer = "Seat not found";
		}
		return buffer;
		
	}

	public String sendAttendant()
	{
		String buffer = null;
		for(Seat seats : seats)
		{

			if(seats.lightOn == true)
			{
				if(buffer == null)
				{
					buffer = "";
				}
				buffer += attendant.pressButton(seats);
				buffer += "\n";
			}

		}
		if(buffer != null)
		{
			return buffer;
		}
		else
		{
			buffer = "\nNo seats have called for an attendant.\n";
		}
		return buffer;

	}

}
